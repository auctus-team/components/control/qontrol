
# Torque control

The following example solves a qp problem expressed at the torque level such that:

\f{equation}{\begin{array}{ccc}\boldsymbol{\tau}^{opt} = & \underset{\boldsymbol{\tau}}{\mathrm{argmin}}  & ||\boldsymbol{\dot{v}}(\boldsymbol{\tau}) - \boldsymbol{\dot{v}}^{target} || + \omega || \boldsymbol{\tau} - (\boldsymbol{g} - \boldsymbol{\dot{q}})||^2_{M^{-1}}\\& \textrm{s.t.} &  \boldsymbol{\tau^{min}} \leq \boldsymbol{\tau} \leq \boldsymbol{\tau^{max}}. \\ & & \boldsymbol{q}^{min} \leq \boldsymbol{q}(\boldsymbol\tau) \leq \boldsymbol{q}^{max} \\ & & \boldsymbol{\dot{q}}^{min} \leq \boldsymbol{\dot{q}}(\boldsymbol\tau) \leq \boldsymbol{\dot{q}}^{max}	\end{array} \f}.

The robot main tasks consists in following a simple trajectory defined in Cartesian space. The mujoco library is used to simulate the robot behaviour.

## Simulation

To run this example run the following command from the `build/examples` directory:

```bash
 ./torqueQontrol robot_name
```

where `robot_name` can be either `panda` or `universal_robots_ur5e`

## Full code 


\include{lineno} torqueQontrol.cpp


## Explanation of the code

### Declaration

First we declare all the objects that will be used to define our problem.

\skipline std::shared_ptr<Model::RobotModel<Model::RobotModelImplType::PINOCCHIO>>

We use pinocchio for our model library.

\skipline torque_problem

The output of our qp controller is at the torque level.

\skipline main_task

We then declare two tasks that will be updated every milliseconds.

The main task is expressed as a Cartesian Acceleration task.

\skipline regularisation_task

And we add a regularisation task (also at the torque level).


### Initialization
\skip initController
\until Model::RobotModel

During initialization we instantiate the model with the robot urdf.

We initialize the problem by giving it the model. By default, the qpmad library is used.

\until regularisation_task

We then fill the task set of torque_problem with the main task and the regularisation task. Each tasks is given a name and a relative weight \f$ \omega \f$. This weight can be modified at any time.


\until joint_torque_constraint

We then fill the constraint set of torque_problem with the three pre-implemented constraints. Each constraint is given a name. These constraints will automatically be updated during the `update` of Qontrol.

\until model->setRobotState(robot_state);

We also create the robot state and fill it with the simulated robot current state.

\until traj

We create a simple trajectory that has been precalculated and store in a csv file. This trajectory start at the robot current Cartesian pose and does a translation of (-0.1, -0,1, -0.1) m.



### Update
\skip updateController
\until  traj->update

The update function is called every milliseconds. At the beginning of each update we fill the new robot state according to the simulated robot. 

We also update the trajectory so that it gives the next Cartesian pose to reach in 1 ms.

\until model->getTipFrameName()).matrix());

We then compute the desired Cartesian acceleration using a simple PD controller. Pinocchio is used to compute the error between the desired Cartesian pose and the current Cartesian pose. This is done by the `log6` function. The `p_gains` and `d_gains` variables are the gains of the PD controller.

\until  model->getInverseJointInertiaMatrix());

The desired Cartesian acceleration is then fed to the `main task`. The regularisation task is also updated so that it compensate for gravity plus a damping term. The resulting regularisation task would be written : \f$  || \boldsymbol{\tau} - (\boldsymbol{g} - \boldsymbol{\dot{q}})||^2_{M^{-1}}\f$

\until };

Once we updated the necassary tasks and constraints we can update the whole problem. If a solution to the problem exist we can then get it and send it to the simulated robot.

### Main function

\until return 0;

The main function function fetches the robot name given in `argv` and starts the Mujoco simulation.


