# Installation

This section details the installation procedure for Qontrol.

@warning Qontrol has only be tested on Ubuntu 20.04 and 22.04. There is no garantee that it will build on other systems.

## Installing dependencies
Before installing Qontrol you need to install the following dependencies

- tinyXML2

    `sudo apt install libtinyxml2-dev`

- Eigen3

    `sudo apt install libeigen3-dev`

- Pinocchio 

    Follow the instructions on [https://stack-of-tasks.github.io/pinocchio/download.html](https://stack-of-tasks.github.io/pinocchio/download.html)

The examples are built using the mujoco simulator. If mujoco is not installed on your system, a cmake FetchContent will be called to compiled it localy for this project.


## Installing Qontrol

1. Clone the git repository

    `git clone https://gitlab.inria.fr/auctus-team/people/lucas-joseph/qontrol.git`

2. Move to Qontrol, create the build directory and go there

    `cd Qontrol && mkdir build && cd build`

3. Run the cmake command

    `cmake ..`

4. Build the library

    `make -j4`


# Launch examples

Go to your build folder and run 
```bash
cd build/examples
./example_name robot_name
```

where:
- `example_name` is the name of the example you want to run. The list of all examples can be found in the `examples` folder.
- `robot_name` can either be `panda` or `universal_robots_ur5e`.

Documented examples can be found [here](@ref md_doc_b-examples_intro)