// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <Qontrol/Solver/GenericSolver.hpp>
#include <qpmad/solver.h>

namespace Qontrol {
namespace Solver {
  /**
   * @brief Implementation of the qpmad solver
   * 
   * @tparam  
   */
template <> class Solver<SolverImplType::qpmad> : public GenericSolver {
public:
  Solver() : GenericSolver{} {};
  ~Solver(){};
  void configure(int optimization_vector_size) override;
  void resizeProblem(qpData qp_data) override;
  bool solve(qpData qp_data) override;
  void setOptions(qpmad::SolverParameters options);
  Eigen::VectorXd getPrimalSolution() override;
  std::shared_ptr<qpmad::Solver> qpmad_ptr_;
private:
  qpmad::SolverParameters options_;
};
} // namespace Solver
} // namespace Qontrol