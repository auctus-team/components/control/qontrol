// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once
#include <plog/Appenders/ConsoleAppender.h>
#include <plog/WinApi.h>

namespace plog { // NO_LINT
template <class Formatter>
class ColorFormatter : public ConsoleAppender<Formatter> {
public:
#ifdef _WIN32
#ifdef _MSC_VER
#pragma warning(suppress : 26812) //  Prefer 'enum class' over 'enum'
#endif
  ColorFormatter(OutputStream outStream = streamStdOut)
      : ConsoleAppender<Formatter>(outStream), m_originalAttr() {
    if (this->m_isatty) {
      CONSOLE_SCREEN_BUFFER_INFO csbiInfo;
      GetConsoleScreenBufferInfo(this->m_outputHandle, &csbiInfo);

      m_originalAttr = csbiInfo.wAttributes;
    }
  }
#else
  ColorFormatter(OutputStream out_stream = streamStdOut)
      : ConsoleAppender<Formatter>(out_stream) {}
#endif

  void write(const Record &record) override {
    util::nstring str = Formatter::format(record);
    util::MutexLock lock(this->m_mutex);

    setColor(record.getSeverity());
    this->writestr(str);
    resetColor();
  }

protected:
  void setColor(Severity severity) {
    if (this->m_isatty) {
      switch (severity) {
#ifdef _WIN32
      case fatal:
        SetConsoleTextAttribute(
            this->m_outputHandle,
            foreground::kRed | foreground::kGreen | foreground::kBlue |
                foreground::kIntensity |
                background::kRed); // white on red background
        break;

      case error:
        SetConsoleTextAttribute(
            this->m_outputHandle,
            static_cast<WORD>(foreground::kRed | foreground::kIntensity |
                              (m_originalAttr & 0xf0))); // red
        break;

      case warning:
        SetConsoleTextAttribute(
            this->m_outputHandle,
            static_cast<WORD>(foreground::kRed | foreground::kGreen |
                              foreground::kIntensity |
                              (m_originalAttr & 0xf0))); // yellow
        break;

      case debug:
      case verbose:
        SetConsoleTextAttribute(
            this->m_outputHandle,
            static_cast<WORD>(foreground::kGreen | foreground::kBlue |
                              foreground::kIntensity |
                              (m_originalAttr & 0xf0))); // cyan
        break;
#else
      case fatal:
        this->m_outputStream << "\x1B[97m\x1B[41m"; // white on red background
        break;

      case error:
        this->m_outputStream << "\x1B[91m"; // red
        break;

      case warning:
        this->m_outputStream << "\x1B[93m"; // yellow
        break;

      case debug:
        this->m_outputStream << "\x1B[92m"; // green
        break;
      case verbose:
        this->m_outputStream << "\x1B[96m"; // cyan
        break;
#endif
      default:
        break;
      }
    }
  }

  void resetColor() {
    if (this->m_isatty) {
#ifdef _WIN32
      SetConsoleTextAttribute(this->m_outputHandle, m_originalAttr);
#else
      this->m_outputStream << "\x1B[0m\x1B[0K";
#endif
    }
  }

private:
#ifdef _WIN32
  WORD m_originalAttr;
#endif
};
} // namespace plog
