// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <Qontrol/Tasks/GenericTask.hpp>
#include <Qontrol/Tasks/CartesianAcceleration/CartesianAcceleration.hpp>
#include <Qontrol/Problem/ControlOutput.hpp>

namespace Qontrol {

namespace Task {

/**
 * @brief \f$ \boldsymbol{\tau}^{opt} =
 * \underset{\boldsymbol{\tau}}{\mathrm{argmin}}  ||
 * \boldsymbol{\dot{v}}(\boldsymbol{\tau}) - \boldsymbol{\dot{v}}^{target}
 * ||^2_{SW} \f$
 *
 *
 * Implementation of a Cartesian acceleration task returning a joint torque
 * command. In this specific case the general task formulation \f$||
 * E\boldsymbol{\tau} - \boldsymbol{f} ||^2_{SW} \f$, can be expressed with
 *
 * \f$ E = J(\boldsymbol{q}) M^{-1}(\boldsymbol{q})\f$  and \f$ \boldsymbol{f} =
 * J(\boldsymbol{q}) M^{-1}(\boldsymbol{q})( \boldsymbol{g}(\boldsymbol{q}) +
 * \boldsymbol{c}(\boldsymbol{q}))
 * - \dot{J}\boldsymbol{\dot{q}} - \boldsymbol{\dot{v}}^{target}\f$ .
 */
template <>
class CartesianAcceleration<Qontrol::ControlOutput::JointTorque>
    : public GenericTask {
public:
  CartesianAcceleration(std::string task_name,
                        std::shared_ptr<Model::GenericModel> model_ptr)
      : GenericTask{task_name, 6, model_ptr} {
    target_acceleration_ = Eigen::VectorXd::Zero(6);
  }

  /**
   * @brief Define the target Cartesian acceleration for this task for the next
   * update.
   *
   * @param target_acceleration \f$\in \mathbb{R}^6\f$
   */
  void setTargetAcceleration(Eigen::Matrix<double, 6, 1> target_acceleration) {
    QontrolChecksize(target_acceleration,target_acceleration_,getName(),"target acceleration");
    target_acceleration_ = target_acceleration;
  }

  void update(double dt) override {
    setE(model_ptr_->getJacobian(model_ptr_->getTipFrameName()) *
         model_ptr_->getInverseJointInertiaMatrix());
    setf(model_ptr_->getJacobian(model_ptr_->getTipFrameName()) *
             model_ptr_->getInverseJointInertiaMatrix() *
             (model_ptr_->getJointCoriolisTorques() +
              model_ptr_->getJointGravityTorques()) +
         target_acceleration_ -
         model_ptr_->getJacobianTimeDerivativeQdot(
             model_ptr_->getTipFrameName()));
  }

private:
  Eigen::VectorXd target_acceleration_;
};
} // namespace Task
} // namespace Qontrol