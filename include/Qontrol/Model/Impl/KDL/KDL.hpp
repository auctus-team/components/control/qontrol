// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.


#pragma once
#ifdef USE_KDL_MODEL

#include "Qontrol/Model/GenericModel.hpp"

// KDL
#include <Qontrol/Model/Impl/KDL/KDL_Eigen_conversions.hpp>
#include <Qontrol/Model/Impl/KDL/kdl_parser.hpp>
#include <kdl/chain.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfdsolver_recursive_newton_euler.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainjnttojacdotsolver.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/jntspaceinertiamatrix.hpp>
#include <kdl/tree.hpp>
#include <utility>

namespace Qontrol {
namespace Model {

class SegmentIndice {
  /* This class is used to bind segment names to the index in the chain */
public:
  auto operator()(const std::string &segment_name) -> const int {
    return this->operator[](segment_name);
  }

  auto operator[](const std::string &segment_name) -> const int {
    if (seg_idx_names.empty()) {
      throw qontrol_exception("Segment idx is empty ! ", __FILE__, __LINE__);
      return -1;
    }
    if (seg_idx_names.count(segment_name) > 0) {
      return seg_idx_names[segment_name];
    }
    throw qontrol_exception("Frame " + segment_name + " doesn't exist",
                            __FILE__, __LINE__);
  }

  void add(const std::string &seg_name, int i) { seg_idx_names[seg_name] = i; }

protected:
  std::map<std::string, int> seg_idx_names;
};

/*!< @brief Implentation of the KDL library */
template <> 
class RobotModel<RobotModelImplType::KDL> : public GenericModel {

protected:
public:
  // Public methods
  static  std::shared_ptr<RobotModel<RobotModelImplType::KDL>> loadModelFromString(const std::string &urdf,
                           std::string root_link = "",
                           std::string tip_link = "");
  static  std::shared_ptr<RobotModel<RobotModelImplType::KDL>> loadModelFromFile(const std::string &urdf_path,
                           std::string root_link = "",
                           std::string tip_link = "");
  std::shared_ptr<RobotModel<RobotModelImplType::KDL>> initializeProblem(std::shared_ptr<RobotModel<RobotModelImplType::KDL>>robot_model, KDLParser kdl_parser, std::string root_link, std::string tip_link);

  auto getRootFrameName() -> std::string override;
  auto getTipFrameName() -> std::string override;
  auto getChain() const -> KDL::Chain;

  auto getNrOfDegreesOfFreedom() -> int override;
  auto getJointGravityTorques() -> Eigen::VectorXd override;
  auto getInverseJointInertiaMatrix() -> Eigen::MatrixXd override;
  auto getJointCoriolisTorques() -> Eigen::VectorXd override;
  auto getSegmentIndex(const std::string &segment_name) -> int;
  auto getFramePose(const std::string &body_name) -> Eigen::Isometry3d override;
  auto getTipFramePose() -> Eigen::Isometry3d override;
  auto getJacobian(const std::string &body_name) -> Eigen::MatrixXd override;
  auto getJacobian(const std::string &body_name,
                   const Eigen::Isometry3d &desired_frame)
      -> Eigen::MatrixXd override;
  auto getJacobianTimeDerivativeQdot(const std::string &body_name)
      -> Eigen::VectorXd override;
  auto getJacobianTimeDerivativeQdot(const std::string &body_name,
                                     const Eigen::Isometry3d &frame)
      -> Eigen::VectorXd override;
  auto getJointInertiaMatrix() -> Eigen::MatrixXd override;
  auto getFrameVelocity(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getFrameVelocity(const std::string &body_name,
                        const Eigen::Isometry3d &desired_frame)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getTipFrameVelocity() -> Eigen::Matrix<double, 6, 1> override;
  void setJointAccelerationFromTorques();
  auto getJointAccelerationFromTorques(Eigen::VectorXd torque) -> Eigen::VectorXd override;

  auto getFrameAccelerationFromTorques(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getFrameAccelerationFromTorques(const std::string &body_name,
                                       const Eigen::Isometry3d &desired_frame)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getFrameAccelerationFromJointAcceleration(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getFrameAccelerationFromJointAcceleration(
      const std::string &body_name, const Eigen::Isometry3d &desired_frame)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getExternalWrench(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getExternalWrench(const std::string &body_name,
                         const Eigen::Isometry3d &frame)
      -> Eigen::Matrix<double, 6, 1> override;
  // Protected methods
private:
  KDL::Tree kdl_tree_;
  KDL::Chain kdl_chain_;
  KDL::JntArrayVel q_in_;
  KDL::JntArray gravity_;
  KDL::JntArray coriolis_;
  KDL::Jacobian jacobian_;
  KDL::JntSpaceInertiaMatrix joint_space_inertia_;
  KDL::FrameVel frame_vel_;
  KDL::Twist jdot_qdot_;
  std::shared_ptr<KDL::ChainFkSolverPos_recursive> fksolver_;
  std::shared_ptr<KDL::ChainJntToJacSolver> chainjacsolver_;
  std::shared_ptr<KDL::ChainDynParam> dyn_model_solver_;
  std::shared_ptr<KDL::ChainFkSolverVel_recursive> fksolvervel_;
  std::shared_ptr<KDL::ChainFdSolver_RNE> fdsolver_;
  std::shared_ptr<KDL::ChainJntToJacDotSolver> chainjnttojacdotsolver_;
  SegmentIndice seg_names_idx_;
  std::string root_link_, tip_link_;
};
} // namespace Model
} // namespace Qontrol
#endif