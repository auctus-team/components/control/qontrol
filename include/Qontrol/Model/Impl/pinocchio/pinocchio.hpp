// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

// Pinocchio
#include <utility>

#include "pinocchio/algorithm/aba.hpp"
#include "pinocchio/algorithm/crba.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/model.hpp"
#include "pinocchio/algorithm/rnea.hpp"
#include "pinocchio/fwd.hpp"
#include "pinocchio/multibody/model.hpp"
#include "pinocchio/parsers/urdf.hpp"

#include "Qontrol/Model/GenericModel.hpp"

namespace Qontrol {
namespace Model {

/*!< @brief Implentation of the pinocchio library */
template <>
class RobotModel<RobotModelImplType::PINOCCHIO> : public GenericModel {
private:
protected:
public:
  auto getModel() const -> pinocchio::Model;
   static  std::shared_ptr<RobotModel<RobotModelImplType::PINOCCHIO>> loadModelFromString(const std::string &urdf,
                           std::string root_link = "",
                           std::string tip_link = "");
  static  std::shared_ptr<RobotModel<RobotModelImplType::PINOCCHIO>> loadModelFromFile(const std::string &urdf_path,
                           std::string root_link = "",
                           std::string tip_link = "");
  std::shared_ptr<RobotModel<RobotModelImplType::PINOCCHIO>> initializeProblem(std::shared_ptr<RobotModel<RobotModelImplType::PINOCCHIO>>robot_model,  std::string root_link, std::string tip_link);
  auto getNrOfDegreesOfFreedom() -> int override;
  auto getRootFrameName() -> std::string override;
  auto getTipFrameName() -> std::string override;
  auto getJointGravityTorques() -> Eigen::VectorXd override;
  auto getJointInertiaMatrix() -> Eigen::MatrixXd override;
  auto getInverseJointInertiaMatrix() -> Eigen::MatrixXd override;
  auto getJointCoriolisTorques() -> Eigen::VectorXd override;

  auto getFramePose(const std::string &body_name) -> Eigen::Isometry3d override;
  auto getTipFramePose() -> Eigen::Isometry3d override;
  auto getFrameVelocity(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getFrameVelocity(const std::string &body_name,
                        const Eigen::Isometry3d &desired_frame)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getTipFrameVelocity() -> Eigen::Matrix<double, 6, 1> override;
  auto getJacobian(const std::string &body_name) -> Eigen::MatrixXd override;
  auto getJacobian(const std::string &body_name,
                   const Eigen::Isometry3d &desired_frame)
      -> Eigen::MatrixXd override;
  auto getJacobianTimeDerivativeQdot(const std::string &body_name)
      -> Eigen::VectorXd override;
  auto getJacobianTimeDerivativeQdot(const std::string &body_name,
                                     const Eigen::Isometry3d &frame)
      -> Eigen::VectorXd override;
  void setJointAccelerationFromTorques();
  auto getJointAccelerationFromTorques(Eigen::VectorXd torque) -> Eigen::VectorXd override;
  auto getFrameAccelerationFromTorques(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getFrameAccelerationFromTorques(const std::string &body_name,
                                       const Eigen::Isometry3d &desired_frame)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getFrameAccelerationFromJointAcceleration(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getFrameAccelerationFromJointAcceleration(
      const std::string &body_name, const Eigen::Isometry3d &desired_frame)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getExternalWrench(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> override;
  auto getExternalWrench(const std::string &body_name,
                         const Eigen::Isometry3d &frame)
      -> Eigen::Matrix<double, 6, 1> override;
  auto frameExist(const std::string &body_name) const -> bool;

private:
  std::string root_link_;
  std::string tip_link_;
  pinocchio::Model model_; /*!< @brief pinocchio model */
  pinocchio::Data data_;   /*!< @brief pinocchio data */
  Eigen::Matrix<double, 6, 1> jdot_qdot_;
};
} // namespace Model
} // namespace Qontrol
