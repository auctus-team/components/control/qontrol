// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once
#include "Qontrol/Utils/Log.hpp"
#include "Qontrol/Utils/Exception.hpp"
#include <Eigen/Dense>
namespace Qontrol {

  /*!< @brief Robot state*/
class RobotState {
public:
  Eigen::VectorXd joint_position;
  Eigen::VectorXd joint_velocity;
  Eigen::VectorXd joint_acceleration;
  Eigen::VectorXd joint_torque;
  Eigen::VectorXd external_joint_torque;

  void resize(int size) {
    joint_position.resize(size);
    joint_velocity.resize(size);
    joint_acceleration.resize(size);
    joint_torque.resize(size);
    external_joint_torque.resize(size);
  }
};

namespace Model {

/** Enum listing the model library that are implemented*/
enum RobotModelImplType { PINOCCHIO, KDL };

/*!< @brief A virtual implementation of a model base library */
class GenericModel {
private:
  std::string robot_name_;
  RobotState robot_state_;

protected:
  GenericModel(){};
  // Public methods
  ~GenericModel(){};
public:
  /**
   * @brief Load the robot model given a urdf path
   * @param urdf_path a urdf string representing the robot description
   * @param root_link The name of the root link in the urdf file. By default the
   * first link in the urdf is taken.
   * @param tip_link The name of the tip link in the urdf file. By default the
   * last link in the urdf is taken.
   */
  // static GenericModel loadModelFromFile(const std::string &urdf_path,
  //                                  std::string root_link = "",
  //                                  std::string tip_link = "") ;

  // static GenericModel loadModelFromString(const std::string &path,
  //                                  std::string root_link = "",
  //                                  std::string tip_link = "");

  // virtual bool loadFromString(const std::string &path,
  //                                  std::string root_link = "",
  //                                  std::string tip_link = "");

  // virtual bool loadFromFile(const std::string &path,
  //                                  std::string root_link = "",
  //                                  std::string tip_link = "");

  /**
   * @brief Sets the robot state (position, velocity, acceleration, and external
   * torque)
   *  @param robot_state the robot state
   */
  void setRobotState(const RobotState &robot_state);

  /**
   * @brief Get the robot state (position, velocity, acceleration, and external
   * torque)
   *  @return The robot state
   */
  auto getRobotState() -> RobotState;

  /**
   * @brief Get the name of the tip frame in the model
   * @return The name of the tip frame
   */
  virtual auto getTipFrameName() -> std::string = 0;

  /**
   * @brief Get the name of the root frame in the model
   *  @return The name of the root frame
   */
  virtual auto getRootFrameName() -> std::string = 0;

  /**
   * @brief Get the robot number of degrees of freedom
   * @return The robot number of degrees of freedom
   */
  virtual auto getNrOfDegreesOfFreedom() -> int = 0;

  /**
   * @brief Set the robot current joint position
   * @param joint_position the robot current joint position
   */
  virtual void setJointPosition(Eigen::VectorXd joint_position);

  /**
   * @brief get the robot current joint position
   * @return A \f$ndof\f$ vector representing the current robot joint position
   */
  virtual auto getJointPosition() -> Eigen::VectorXd;

  /**
   * @brief Set the robot lower joint position limits
   *  @param lim The robot lower position limits
   * @remark This functions is automatically called by loadModelFromString to
   * set the lower joint position limits according to the urdf model
   */
  void setLowerJointPositionLimits(Eigen::VectorXd lim);

  /**
   *  @brief Set the robot upper joint position limits
   *  @param lim The robot upper position limits
   * @remark This functions is automatically called by loadModelFromString to
   * set the upper joint position limits according to the urdf model
   */
  void setUpperJointPositionLimits(Eigen::VectorXd lim);

  /**
   *  @brief Get the robot lower joint position limits
   *  @return The robot lower position limits
   */
  virtual auto getLowerJointPositionLimits() -> Eigen::VectorXd;

  /**
   *  @brief Get the robot upper joint position limits
   *  @return The robot upper position limits
   */
  virtual auto getUpperJointPositionLimits() -> Eigen::VectorXd;

  /**
   * @brief Set the robot current joint velocity
   * @param joint_velocity the robot current joint velocity
   */
  virtual void setJointVelocity(Eigen::VectorXd joint_velocity);

  /**
   * @brief Get the robot current joint velocity
   * @return A \f$ndof\f$ vector representing the current robot joint velocity
   */
  virtual auto getJointVelocity() -> Eigen::VectorXd;

  /**
   * @brief Set the robot joint velocity limts
   * @param lim The robot velocity limits
   * @remark This functions is automatically called by loadModelFromString to
   * set the joint velocity limits according to the urdf model
   */
  void setJointVelocityLimits(Eigen::VectorXd lim);

  /**
   * @brief Get the robot joint velocity limts
   *  @return The robot velocity limits
   */
  auto getJointVelocityLimits() -> Eigen::VectorXd;

  /**
   * @brief Set the robot current joint acceleration
   * @param joint_acceleration the robot current joint acceleration
   */
  virtual void setJointAcceleration(Eigen::VectorXd joint_acceleration);

  /**
   * @brief Get the robot current joint acceleration
   * @return A \f$ndof\f$ vector representing the current robot joint
   * acceleration
   */
  virtual auto getJointAcceleration() -> Eigen::VectorXd;

  /**
   * @brief Set the robot current joint torque
   * @param joint_torque the robot current joint torque
   */
  virtual void setJointTorque(Eigen::VectorXd joint_torque);

  /**
   * @brief Get the robot current joint torque
   * @return A \f$ndof\f$ vector representing the current robot joint
   * torque
   */
  virtual auto getJointTorque() -> Eigen::VectorXd;

  /**
   * @brief Set the robot joint torque limits
   * @param lim The robot upper velocity limits
   * @remark This functions is automatically called by loadModelFromString to
   * set the joint torque limits according to the urdf model
   */
  void setJointTorqueLimits(Eigen::VectorXd lim);

  /**
   * @brief Get the robot joint torque limits
   *  @return The robot torque limits
   */
  auto getJointTorqueLimits() -> Eigen::VectorXd;

  /**
   * @brief Set the robot current external joint torque
   * @param external_joint_torque the robot external joint torque
   */
  virtual void setExternalJointTorque(Eigen::VectorXd external_joint_torque);

  /**
   * @brief Get the robot current external joint torque
   * @return A \f$ndof\f$ vector representing the external torque measured on
   * each joint (in Nm for a revolute joint and N for a prismatic joint )
   */
  virtual auto getExternalJointTorque() -> Eigen::VectorXd;

  /**
   * @brief Get the vector representing the gravity effect associated to
   * the \f$g\f$ term in the equation of motion: \f$ M(q)\ddot{q} + c(q,
   * \dot{q}) + g(q) = \tau \f$. Taken at the current robot state.
   *  @return A f$ndof\f$ vector .
   */
  virtual auto getJointGravityTorques() -> Eigen::VectorXd = 0;

  /**
   * @brief Get the vector representing the Coriolis effect associated to
   * the \f$c(q, \dot{q})\f$ term in the equation of motion: \f$ M(q)\ddot{q} +
   * c(q, \dot{q}) + g(q) = \tau \f$. Taken at the current robot state.
   *  @return A \f$ndof\f$ vector.
   */
  virtual auto getJointCoriolisTorques() -> Eigen::VectorXd = 0;

  /**
   * @brief Get the matrix representing the joint space
   * inertia matrix associated to the \f$M(q)\f$ term in the equation of motion:
   * \f$ M(q)\ddot{q} + c(q, \dot{q}) + g(q) = \tau \f$. Taken at the current
   * robot state.
   *  @return A \f$ndof \times ndof\f$ matrix
   */
  virtual auto getJointInertiaMatrix() -> Eigen::MatrixXd = 0;

  /**
   * @brief Get the matrix representing the inverse of the
   * joint space inertia matrix associated to the \f$M(q)\f$ term in the
   * equation of motion: \f$ M(q)\ddot{q} + c(q, \dot{q}) + g(q) = \tau \f$.
   * Taken at the current robot state.
   *  @return A \f$ndof \times ndof\f$ matrix
   */
  virtual auto getInverseJointInertiaMatrix() -> Eigen::MatrixXd = 0;

  /**
   * @brief Get the pose of the frame called \f$\text_it{body_name}\f$ in the
   * URDF wrt. the root frame.
   *  @param body_name The name of the frame in the urdf.
   *  @return an Isometry.
   */
  virtual auto getFramePose(const std::string &body_name)
      -> Eigen::Isometry3d = 0;

  /**
   * @brief Get the pose of the tip frame wrt. the root frame.
   * @return an Isometry.
   */
  virtual auto getTipFramePose() -> Eigen::Isometry3d = 0;

  /**
   * @brief Get the Jacobian matrix expressed at the frame name
   * \f$\textit{body_name}\f$ in the URDF wrt. the root frame.
   *  @param body_name The name of the frame in the urdf
   *  @return A \f$6 \times ndof\f$ Jacobian matrix.
   */
  virtual auto getJacobian(const std::string &body_name) -> Eigen::MatrixXd = 0;

  /**
   *  @param frame A frame expressed wrt. the root frame.
   *  @return A \f$6 \times ndof\f$ Jacobian matrix expressed at the
   * \f$\textit{frame}\f$ pose wrt. the root frame.
   */
  virtual auto getJacobian(const std::string &body_name,
                           const Eigen::Isometry3d &frame)
      -> Eigen::MatrixXd = 0;

  /**
   * @brief Get the vector representing the \f$\dot{J}\dot{q}\f$ terme in
   * the equation \f$\dot{twist} = \dot{J}\dot{q} + J\ddot{q}\f$.
   * @param body_name The name of the frame in the urdf
   * @return A \f$6\f$ vector
   */
  virtual auto getJacobianTimeDerivativeQdot(const std::string &body_name)
      -> Eigen::VectorXd = 0;

  /**
   * @brief Get the vector representing the \f$\dot{J}\dot{q}\f$ terme in
   * the equation \f$\dot{twist} = \dot{J}\dot{q} + J\ddot{q}\f$.
   * @param body_name The name of the frame in the urdf
   * @return A \f$6\f$ vector
   */
  virtual auto getJacobianTimeDerivativeQdot(const std::string &body_name,
                                             const Eigen::Isometry3d &frame)
      -> Eigen::VectorXd = 0;

  /**
   * @brief Get the twist representing the velocity of the frame named
   * \f$\textit{body_name}\f$ in the URDF wrt. the root frame.
   * @param body_name The name of the frame in the urdf.
   * @return a twist.
   */
  virtual auto getFrameVelocity(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> = 0;

  /**
   * @brief Get the twist representing the \f$\textit{frame}\f$ velocity
   * wrt. the root frame.
   *  @param frame A frame expressed wrt. the root frame.
   *  @return A twist
   */
  virtual auto getFrameVelocity(const std::string &body_name,
                                const Eigen::Isometry3d &frame)
      -> Eigen::Matrix<double, 6, 1> = 0;

  /**
   * @brief The twist representing the velocity of the tip frame
   * wrt. the root frame.
   *  @return A twist
   */
  virtual auto getTipFrameVelocity() -> Eigen::Matrix<double, 6, 1> = 0;

  virtual auto getJointAccelerationFromTorques(Eigen::VectorXd torque) -> Eigen::VectorXd = 0;

  /**
   * @brief Get the acceleration of the frame named \f$\textit{body_name}\f$ in
   * the URDF wrt. the root frame given the robot current joint
   * position/velocity/acceleration
   * @param body_name The name of the frame in the urdf.
   * @return Eigen::Vector6d
   */
  virtual auto
  getFrameAccelerationFromJointAcceleration(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> = 0;

  /**
   * @brief Get the acceleration of the frame named \f$\textit{body_name}\f$ in
   * the URDF wrt. the root frame given the robot current joint
   * position/velocity/acceleration
   * @param body_name The name of the frame in the urdf.
   * @return Eigen::Vector6d
   */
  virtual auto
  getFrameAccelerationFromJointAcceleration(const std::string &body_name,
                                            const Eigen::Isometry3d &frame)
      -> Eigen::Matrix<double, 6, 1> = 0;

  /**
   * @brief Get the acceleration of the frame named \f$\textit{body_name}\f$ in
   * the URDF wrt. the root frame given the robot current joint
   * position/velocity/torque/external torque
   * @param body_name The name of the frame in the urdf.
   * @return Eigen::Vector6d
   */
  virtual auto getFrameAccelerationFromTorques(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> = 0;

  /**
   * @brief Get the acceleration of the frame named \f$\textit{body_name}\f$ in
   * the URDF wrt. the root frame given the robot current joint
   * position/velocity/torque/external torque
   * @param body_name The name of the frame in the urdf.
   * @return Eigen::Vector6d
   */
  virtual auto getFrameAccelerationFromTorques(const std::string &body_name,
                                               const Eigen::Isometry3d &frame)
      -> Eigen::Matrix<double, 6, 1> = 0;

  // virtual Eigen::Matrix<double, 6, 1> getTipFrameAcceleration() = 0;

  /**
   * @brief Get the external wrench applied on the named
   * \f$\textit{body_name}\f$ in the URDF wrt. the root frame given the
   * external joint torques applied on the robot
   * @param body_name The name of the frame in the urdf.
   *  @return Eigen::Vector6d
   */
  virtual auto getExternalWrench(const std::string &body_name)
      -> Eigen::Matrix<double, 6, 1> = 0;

  virtual auto getExternalWrench(const std::string &body_name,
                                 const Eigen::Isometry3d &frame)
      -> Eigen::Matrix<double, 6, 1> = 0;

  /**
   * @brief compute the skew symetry matrix of a vector. Usefull to perform
   * cross products
   *  @param vector a vector
   *  @return The skew matrix associated to \f$vecotr\f$
   */
  static auto skew(Eigen::Vector3d vector) -> Eigen::Matrix3d;

  auto integrate(Eigen::VectorXd q,Eigen::VectorXd v,double dt) -> Eigen::VectorXd;
  // Protected methods
protected:
  Eigen::VectorXd lower_joint_position_limits;
  Eigen::VectorXd upper_joint_position_limits;
  Eigen::VectorXd joint_velocity_limits;
  Eigen::VectorXd joint_torque_limits;
  // Private methods
private:
};

/**
 * @brief Decalaration of a template specialization for the model library
 * 
 * @tparam model_lib 
 */
template <RobotModelImplType model_lib> class RobotModel {};

} // namespace Model
} // namespace Qontrol
