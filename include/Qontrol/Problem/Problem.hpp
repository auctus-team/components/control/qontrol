// This file is part of Qontrol, a quadratic optimization library to
// control robot.
//
// Copyright (C) 2023 Lucas Joseph <lucas.joseph@inria.fr>
//
// Qontrol is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// Alternatively, you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 3 of
// the License, or (at your option) any later version.
//
// Qontrol is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License or the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License and a copy of the GNU General Public License along with
// Qontrol. If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include "Eigen/Dense"

#include "Qontrol/Problem/ControlOutput.hpp"

#include <Qontrol/Constraints/ConstraintSet.hpp>

#include "Qontrol/Model/GenericModel.hpp"

#include <Qontrol/Solver/GenericSolver.hpp>
#include <Qontrol/Solver/Impl/qpmad/qpmad.hpp>

#include <Qontrol/Tasks/TaskSet.hpp>

#include "Qontrol/Utils/Log.hpp"
#include <vector>
namespace Qontrol {
  /**
   * @brief Define the control problem
   * 
   * Where you handle the task set and constraint set and get the solution
   * to the control problem
   * 
   * @tparam output a ControlOutput
   */
template <ControlOutput output> class Problem {

protected:
  ControlOutput control_output_;
  std::shared_ptr<Model::GenericModel> model_ptr_;
  Solver::qpData qp_data_;
  bool solution_found_ = false;
  double dt_;
public:
  std::shared_ptr<Solver::GenericSolver> solver_ptr_;
  std::shared_ptr<Task::TaskSet<output>> task_set;
  std::shared_ptr<Constraint::ConstraintSet<output>> constraint_set;

  /**
   * @brief Construct a new Problem
   *
   * Setting the dimension of the problem to the number of degrees of freedom of the robot (given by the model)
   * Intialize the qp solver object with the dimension of the problem
   * Create the task set with the dimension of the problem
   * Create the constraint set with the dimension of the problem
   * 
   * @tparam ModelLibrary 
   * @tparam SolverLibrary
   * @param model_library  a pointer of the model library used. This point is implicitly cast to a GenericModel pointer
   * @param solver_library a pointer of the solver library used. This point is implicitly cast to a GenericSolver pointer
   */
  template <class ModelLibrary, class SolverLibrary>
  Problem(std::shared_ptr<ModelLibrary> model_library,
          std::shared_ptr<SolverLibrary> solver_library)
      : model_ptr_{model_library}, solver_ptr_{solver_library} {
    solver_ptr_->configure(model_ptr_->getNrOfDegreesOfFreedom());
    task_set = std::make_shared<Task::TaskSet<output>>(model_ptr_);
    constraint_set =
        std::make_shared<Constraint::ConstraintSet<output>>(model_ptr_);
  };

  /**
   * @brief Construct a new Problem using the qpOASES qp solver by default
   * 
    * Setting the dimension of the problem to the number of degrees of freedom of the robot (given by the model)
   * Intialize the qp solver object with the dimension of the problem
   * Create the task set with the dimension of the problem
   * Create the constraint set with the dimension of the problem
   * 
   * @tparam ModelLibrary a pointer of the model library used. This point is implicitly cast to a GenericModel pointer
   * @param model_library a pointer of the solver library used. This point is implicitly cast to a GenericSolver pointer
   */
  template <class ModelLibrary>
  Problem(std::shared_ptr<ModelLibrary> model_library) : model_ptr_{model_library} {
    auto solver =
        std::make_shared<Solver::Solver<Solver::SolverImplType::qpmad>>();
    solver->configure(model_ptr_->getNrOfDegreesOfFreedom());
    solver_ptr_ = std::static_pointer_cast<Solver::GenericSolver>(solver);

    task_set = std::make_shared<Task::TaskSet<output>>(model_ptr_);
    constraint_set =
        std::make_shared<Constraint::ConstraintSet<output>>(model_ptr_);
  };

  void updateTasks(double dt)
  {
    // Reset previous hessian and gradient
    qp_data_.hessian.setZero();
    qp_data_.gradient.setZero();

    // For each task in the task set:
    //    call the update function to update its hessian and gradient
    //    get the task hessian and relative weight and add it to the global hessain
    //    get the task gradient and relative weight and add it to the global gradient
    int i = 0;
    for (auto &task : task_set->getTasks()) {
      task->update(dt);
      qp_data_.hessian += task_set->getTasksWeight().at(i) * task->getHessian();
      qp_data_.gradient +=
          task_set->getTasksWeight().at(i) * task->getGradient();
      i++;
    }
  }

  void updateConstraints(double dt)
  {
    // For each constraint in the constraint set:
    //    call the update function to upate its constraint matrix and bounds
    //    insert the constraint matrix to the global constraint matrix
    //    insert the upper and lower bounds to their respective global constraint vector
    int inserted_constraints = 0;
    for (auto &constraint : constraint_set->getConstraints()) {
      constraint->update(dt);
      // If there are move constraint than in the previous problem do a conservative resize
      // before inserting the constraints
      if (inserted_constraints + constraint->getSize() > qp_data_.lb_a.rows()) {
        qp_data_.a_constraints.conservativeResize(
            inserted_constraints + constraint->getSize() , Eigen::NoChange);
        qp_data_.ub_a.conservativeResize(inserted_constraints + constraint->getSize() );
        qp_data_.lb_a.conservativeResize(inserted_constraints + constraint->getSize() );
      }
      qp_data_.a_constraints.block(inserted_constraints, 0,
                                   constraint->getSize(),
                                   model_ptr_->getNrOfDegreesOfFreedom()) =
          constraint->getConstraintMatrix();
      qp_data_.ub_a.segment(inserted_constraints, constraint->getSize()) =
          constraint->getUpperBounds();
      qp_data_.lb_a.segment(inserted_constraints, constraint->getSize()) =
          constraint->getLowerBounds();
      // increment the number of constraint added to check if we need to resize the problem
      inserted_constraints += constraint->getSize();
    }
    // Check if final size is less than previous one (ie some constraint were
    // removed). If yes resize the problem
    if (inserted_constraints < solver_ptr_->getProblemData().lb_a.rows()) {
      qp_data_.ub_a.conservativeResize(inserted_constraints);
      qp_data_.lb_a.conservativeResize(inserted_constraints);
      qp_data_.a_constraints.conservativeResize(inserted_constraints,
                                                Eigen::NoChange);
    }
  }

  /**
   * @brief update the problem
   * 
   * Compute the new hessian and gradient matrices for each task in the task set and sum it. 
   * Concatenate all the constraints matrices in the constraint set to form a single constraint matrice.
   * Concatenate all the upper bounds and lower bound vectors in the constraint set to form an upper bound vector and a lower bound vector
   * Resize the qp solver problem if the number of constraint has changed since the last update
   * 
   * @param frequency 
   */
  void update(double dt = 0.001) {
    dt_ = dt;
    solution_found_ = false;
    // get the previous problem qpData object to know the previous problem sizes
    qp_data_ = solver_ptr_->getProblemData();
    updateTasks(dt);
    updateConstraints(dt);
    solution_found_ = solver_ptr_->solve(qp_data_);
  }

  auto solutionFound() -> bool {
    return solution_found_;
  }

  /**
   * @brief Get the solution from the qp solver
   * 
   * @return Eigen::VectorXd the solution to the problem
   */
  auto getPrimalSolution() -> Eigen::VectorXd { return solver_ptr_->getPrimalSolution();};

  /**
   * @brief  Get the problem control output type
   * 
   * @return ControlOutput 
   */
  ControlOutput getControlOutput() { return control_output_; };
};

} // namespace Qontrol
