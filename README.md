# Qontrol

Qontrol is a library aiming to simplify the expression of control problem of a robotic manipulator using Quadratic Optimization formulation.

This library is voluntarily modular so that you can easily use other qp solver but also other model libraries.

## Installation

Qontrol has been tested on Ubuntu. See the following installation instruction [here](https://auctus-team.gitlabpages.inria.fr/components/control/qontrol/md_doc_installation.html)

## Philosophy

The philosophy behind Qontrol can be found [here](https://auctus-team.gitlabpages.inria.fr/components/control/qontrol/md_doc_phylosophy.html)

## Examples

Use cas examples of Qontrol can be found [here](https://auctus-team.gitlabpages.inria.fr/components/control/qontrol/md_doc_b-examples_intro.html)

These examples showcase the functionnalities of Qontrol with a simulated robot using MuJoCo.