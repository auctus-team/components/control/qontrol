#!/bin/bash

cd $1
~/soft/catkin_build/catkin.sh build --this -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_BUILD_TYPE=Release
python3 $2/.vscode/compile_json.py;
