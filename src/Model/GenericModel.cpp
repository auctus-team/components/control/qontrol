#include <utility>

#include "Qontrol/Model/GenericModel.hpp"

namespace Qontrol {
namespace Model {

void GenericModel::setRobotState(const RobotState &robot_state) {
  robot_state_ = robot_state;
}

auto GenericModel::getRobotState() -> RobotState {
  // @todo
  return robot_state_;
}

void GenericModel::setJointPosition(Eigen::VectorXd joint_position) {
  robot_state_.joint_position = std::move(joint_position);
}

auto GenericModel::getJointPosition() -> Eigen::VectorXd {
  // @todo
  return robot_state_.joint_position;
}

void GenericModel::setLowerJointPositionLimits(Eigen::VectorXd lim) {
  lower_joint_position_limits = std::move(lim);
}

void GenericModel::setUpperJointPositionLimits(Eigen::VectorXd lim) {
  upper_joint_position_limits = std::move(lim);
}

auto GenericModel::getLowerJointPositionLimits() -> Eigen::VectorXd {
  return lower_joint_position_limits;
}

auto GenericModel::getUpperJointPositionLimits() -> Eigen::VectorXd {
  return upper_joint_position_limits;
}

void GenericModel::setJointVelocity(Eigen::VectorXd joint_velocity) {
  robot_state_.joint_velocity = std::move(joint_velocity);
}

auto GenericModel::getJointVelocity() -> Eigen::VectorXd {
  return robot_state_.joint_velocity;
}

void GenericModel::setJointVelocityLimits(Eigen::VectorXd lim) {
  joint_velocity_limits = std::move(lim);
}

auto GenericModel::getJointVelocityLimits() -> Eigen::VectorXd {
  return joint_velocity_limits;
}

void GenericModel::setJointAcceleration(Eigen::VectorXd joint_acceleration) {
  robot_state_.joint_acceleration = std::move(joint_acceleration);
}

auto GenericModel::getJointAcceleration() -> Eigen::VectorXd {
  // @todo
  return robot_state_.joint_acceleration;
}

void GenericModel::setJointTorque(Eigen::VectorXd joint_torque) {
  robot_state_.joint_torque = std::move(joint_torque);
}

auto GenericModel::getJointTorque() -> Eigen::VectorXd {
  return robot_state_.joint_torque;
}

void GenericModel::setJointTorqueLimits(Eigen::VectorXd lim) {
  joint_torque_limits = std::move(lim);
}

auto GenericModel::getJointTorqueLimits() -> Eigen::VectorXd {
  // @todo
  return joint_torque_limits;
}

void GenericModel::setExternalJointTorque(
    Eigen::VectorXd external_joint_torque) {
  robot_state_.external_joint_torque = std::move(external_joint_torque);
}

auto GenericModel::getExternalJointTorque() -> Eigen::VectorXd {
  // @todo
  return robot_state_.external_joint_torque;
}

auto GenericModel::skew(Eigen::Vector3d vector) -> Eigen::Matrix3d {
  double a = vector[0];
  double b = vector[1];
  double c = vector[2];
  Eigen::Matrix3d skew_mat;
  skew_mat << 0, -c, b, c, 0, -a, -b, a, 0;
  return skew_mat;
}

auto GenericModel::integrate(Eigen::VectorXd q,Eigen::VectorXd v,double dt) -> Eigen::VectorXd
{
  return q += v*dt;
}

} // namespace Model
} // namespace Qontrol
