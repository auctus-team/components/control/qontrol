/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2008, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of the Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Wim Meeussen */
#if USE_KDL_MODEL
#include <Qontrol/Model/Impl/KDL/kdl_parser.hpp>

#include <string>
#include <vector>

#include <urdf_model/model.h>
#include <urdf_parser/urdf_parser.h>

#include <kdl/frames_io.hpp>

#ifdef HAS_URDF
#include <urdf/model.h>
#include <urdf/urdfdom_compatibility.h>
#endif

namespace Qontrol {
namespace Model {
// construct vector
auto KDLParser::toKdl(urdf::Vector3 v) -> KDL::Vector {
  return KDL::Vector(v.x, v.y, v.z);
}

// construct rotation
auto KDLParser::toKdl(urdf::Rotation r) -> KDL::Rotation {
  return KDL::Rotation::Quaternion(r.x, r.y, r.z, r.w);
}

// construct pose
auto KDLParser::toKdl(urdf::Pose p) -> KDL::Frame {
  return KDL::Frame(toKdl(p.rotation), toKdl(p.position));
}

// construct joint
auto KDLParser::toKdl(const urdf::JointSharedPtr &jnt) -> KDL::Joint {
  KDL::Frame f_parent_jnt = toKdl(jnt->parent_to_joint_origin_transform);

  switch (jnt->type) {
  case urdf::Joint::FIXED: {
    return KDL::Joint(jnt->name, KDL::Joint::None);
  }
  case urdf::Joint::REVOLUTE: {
    KDL::Vector axis = toKdl(jnt->axis);
    return KDL::Joint(jnt->name, f_parent_jnt.p, f_parent_jnt.M * axis,
                      KDL::Joint::RotAxis);
  }
  case urdf::Joint::CONTINUOUS: {
    KDL::Vector axis = toKdl(jnt->axis);
    return KDL::Joint(jnt->name, f_parent_jnt.p, f_parent_jnt.M * axis,
                      KDL::Joint::RotAxis);
  }
  case urdf::Joint::PRISMATIC: {
    KDL::Vector axis = toKdl(jnt->axis);
    return KDL::Joint(jnt->name, f_parent_jnt.p, f_parent_jnt.M * axis,
                      KDL::Joint::TransAxis);
  }
  default: {
    PLOGW << "Converting unknown joint type of joint " << jnt->name.c_str()
          << " into a fixed joint";
    return KDL::Joint(jnt->name, KDL::Joint::None);
  }
  }
  return KDL::Joint();
}

// construct inertia
auto KDLParser::toKdl(const urdf::InertialSharedPtr &i)
    -> KDL::RigidBodyInertia {
  KDL::Frame origin = toKdl(i->origin);

  // the mass is frame independent
  double kdl_mass = i->mass;

  // kdl and urdf both specify the com position in the reference frame of the
  // link
  KDL::Vector kdl_com = origin.p;

  // kdl specifies the inertia matrix in the reference frame of the link,
  // while the urdf specifies the inertia matrix in the inertia reference frame
  KDL::RotationalInertia urdf_inertia =
      KDL::RotationalInertia(i->ixx, i->iyy, i->izz, i->ixy, i->ixz, i->iyz);

  // Rotation operators are not defined for rotational inertia,
  // so we use the RigidBodyInertia operators (with com = 0) as a workaround
  KDL::RigidBodyInertia kdl_inertia_wrt_com_workaround =
      origin.M * KDL::RigidBodyInertia(0, KDL::Vector::Zero(), urdf_inertia);

  // Note that the RigidBodyInertia constructor takes the 3d inertia wrt the com
  // while the getRotationalInertia method returns the 3d inertia wrt the frame
  // origin (but having com = Vector::Zero() in kdl_inertia_wrt_com_workaround
  // they match)
  KDL::RotationalInertia kdl_inertia_wrt_com =
      kdl_inertia_wrt_com_workaround.getRotationalInertia();

  return KDL::RigidBodyInertia(kdl_mass, kdl_com, kdl_inertia_wrt_com);
}

// recursive function to walk through tree
auto KDLParser::addChildrenToTree(const urdf::LinkConstSharedPtr &root,
                                  KDL::Tree &tree) -> bool {
  std::vector<urdf::LinkSharedPtr> children = root->child_links;
  // PLOGD << "Link " << root->name.c_str() << " had " << children.size()
  //       << "children";

  // constructs the optional inertia
  KDL::RigidBodyInertia inert(0);
  if (root->inertial) {
    inert = toKdl(root->inertial);
  }

  // constructs the kdl joint
  KDL::Joint jnt = toKdl(root->parent_joint);

  // construct the kdl segment
  KDL::Segment sgm(root->name, jnt,
                   toKdl(root->parent_joint->parent_to_joint_origin_transform),
                   inert);

  // add segment to tree
  tree.addSegment(sgm, root->parent_joint->parent_link_name);

  // recurslively add all children
  for (auto &i : children) {
    PLOGD << " Adding children " << i->name;
    if (!addChildrenToTree(i, tree)) {
      return false;
    }
  }
  return true;
}

auto KDLParser::treeFromFile(const std::string &file, KDL::Tree &tree) -> bool {
  auto robot_model_ptr = urdf::parseURDFFile(file);
  return treeFromUrdfModel(*robot_model_ptr, tree);
}

auto KDLParser::treeFromString(const std::string &xml, KDL::Tree &tree)
    -> bool {
  auto robot_model_ptr = urdf::parseURDF(xml);
  if (!robot_model_ptr) {
    PLOGE << "Could not generate robot model";
    return false;
  }
  return treeFromUrdfModel(*robot_model_ptr, tree);
}

auto KDLParser::treeFromXml(const tinyxml2::XMLDocument *xml_doc,
                            KDL::Tree &tree) -> bool {
  if (xml_doc == nullptr) {
    PLOGE << "Could not parse the xml document";
    return false;
  }

  tinyxml2::XMLPrinter printer;
  xml_doc->Print(&printer);

  return treeFromString(printer.CStr(), tree);
}

auto KDLParser::treeFromUrdfModel(urdf::ModelInterface &robot_model,
                                  KDL::Tree &tree) -> bool {
  robot_model_ = robot_model;
  if (!robot_model_.getRoot()) {
    return false;
  }
  lower_position_limit_.resize(0);
  upper_position_limit_.resize(0);
  velocity_limit_.resize(0);
  torque_limit_.resize(0);
  tree = KDL::Tree(robot_model_.getRoot()->name);

  // warn if root link has inertia. KDL does not support this
  if (robot_model_.getRoot()->inertial) {
    PLOGW << "The root link " << robot_model_.getRoot()->name.c_str()
          << " has an inertia specified in the URDF, but KDL does not "
             "support a root link with an inertia.  As a workaround, you can "
             "add an extra "
             "dummy link to your URDF.";
  }

  //  add all children
  for (size_t i = 0; i < robot_model_.getRoot()->child_links.size(); i++) {
    if (!addChildrenToTree(robot_model_.getRoot()->child_links[i], tree)) {
      return false;
    }
  }

  return true;
}

auto KDLParser::getJointUpperPositionLimits(KDL::Chain chain)
    -> Eigen::VectorXd {
  for (int i = 0; i < chain.segments.size(); ++i) {
    if (robot_model_.getLink(chain.getSegment(i).getName())->parent_joint &&
        robot_model_.getLink(chain.getSegment(i).getName())
            ->parent_joint->limits) {
      int index = upper_position_limit_.size();
      upper_position_limit_.conservativeResize(index + 1);

      upper_position_limit_(index) =
          robot_model_.getLink(chain.getSegment(i).getName())
              ->parent_joint->limits->upper;
    }
  }
  return upper_position_limit_;
}

auto KDLParser::getJointLowerPositionLimits(KDL::Chain chain)
    -> Eigen::VectorXd {
  for (int i = 0; i < chain.segments.size(); ++i) {
    if (robot_model_.getLink(chain.getSegment(i).getName())->parent_joint &&
        robot_model_.getLink(chain.getSegment(i).getName())
            ->parent_joint->limits) {
      int index = lower_position_limit_.size();
      lower_position_limit_.conservativeResize(index + 1);
      lower_position_limit_(index) =
          robot_model_.getLink(chain.getSegment(i).getName())
              ->parent_joint->limits->lower;
    }
  }
  return lower_position_limit_;
}

auto KDLParser::getJointVelocityLimits(KDL::Chain chain) -> Eigen::VectorXd {
  for (int i = 0; i < chain.segments.size(); ++i) {
    if (robot_model_.getLink(chain.getSegment(i).getName())->parent_joint &&
        robot_model_.getLink(chain.getSegment(i).getName())
            ->parent_joint->limits) {
      int index = velocity_limit_.size();
      velocity_limit_.conservativeResize(index + 1);

      velocity_limit_(index) =
          robot_model_.getLink(chain.getSegment(i).getName())
              ->parent_joint->limits->velocity;
    }
  }
  return velocity_limit_;
}

auto KDLParser::getJointTorqueLimits(KDL::Chain chain) -> Eigen::VectorXd {
  for (int i = 0; i < chain.segments.size(); ++i) {
    if (robot_model_.getLink(chain.getSegment(i).getName())->parent_joint &&
        robot_model_.getLink(chain.getSegment(i).getName())
            ->parent_joint->limits) {
      int index = torque_limit_.size();
      torque_limit_.conservativeResize(index + 1);

      torque_limit_(index) =
          robot_model_.getLink(chain.getSegment(i).getName())
              ->parent_joint->limits->effort;
    }
  }
  return torque_limit_;
}

} // namespace Model
} // namespace Qontrol
#endif