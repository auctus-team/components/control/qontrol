#include <Qontrol/Solver/GenericSolver.hpp>

namespace Qontrol {
namespace Solver {

void GenericSolver::checkProblem(qpData qp_data) {
  // First check if vector are of same size
  if (Size(qp_data.ub_a) != Size(qp_data.lb_a)) {
    std::invalid_argument(
        "Constraints upper bound and lower bound are not of the same size");
  }

  //Check if lb_a <= ub_a
  bool is_less = true;
  for (int row=0; row<qp_data.lb_a.rows(); ++ row)
  {
    is_less &= qp_data.lb_a[row] <= qp_data.ub_a[row];
  }
  if ( !is_less ){
     std::invalid_argument(
        "Constraints lower bound is greater than its upper bound");
  }

  // Check if constraint matrix has correct number of cols
  if (qp_data.ub_a.rows() != qp_data.a_constraints.rows()) {
    std::invalid_argument(
        "Constraints bounds and constraints matrix are of inconsistent size");
  }

  // Check if constraint matrix has a correct number of rows
  if (qp_data.a_constraints.cols() != qp_data.hessian.cols()) {
    std::invalid_argument("Constraints matrix has inconsistent size with the "
                  "optimization variable size");
  }


  // Check if constraints size is the same as the previous one
  // If not resize the problem
  if (Size(qp_data.lb_a) != Size(qp_data_.lb_a)) {
    resizeProblem(qp_data);
    PLOGI << "Constraint size is different, resizing solver problem";
  }
}
} // namespace Solver
} // namespace Qontrol