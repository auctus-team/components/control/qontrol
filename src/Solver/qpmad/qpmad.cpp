// #ifdef USE_QPMAD_SOLVER
#include <Qontrol/Solver/Impl/qpmad/qpmad.hpp>

    namespace Qontrol {
  namespace Solver {
  void Solver<SolverImplType::qpmad>::configure(int optimization_vector_size) {
    // We start with an empty constraint set
    number_of_constraints = 0;
    qpmad_ptr_ = std::make_shared<qpmad::Solver>();

    qp_data_.hessian.resize(optimization_vector_size,
                                optimization_vector_size);
    qp_data_.gradient.resize(optimization_vector_size);
    qp_data_.a_constraints.resize(number_of_constraints,
                                      optimization_vector_size);
    qp_data_.lb_a.resize(number_of_constraints);
    qp_data_.ub_a.resize(number_of_constraints);
    qp_data_.solution.resize(optimization_vector_size);
  }

  void Solver<SolverImplType::qpmad>::resizeProblem(qpData qp_data) {
    // Nothing to be done with qpmad
  }

  auto Solver<SolverImplType::qpmad>::solve(qpData qp_data) -> bool {
    checkProblem(qp_data);
    qp_data_ = qp_data;
    qpmad::Solver::ReturnStatus status =
        qpmad_ptr_->solve(qp_data_.solution, qp_data_.hessian, qp_data_.gradient,
                          qp_data_.a_constraints, qp_data_.lb_a, qp_data_.ub_a,options_);

    return status == qpmad::Solver::OK;
  }

  // Set qpmad solver options
  void Solver<SolverImplType::qpmad>::setOptions(qpmad::SolverParameters options) {
    options_ = options;
  }

  auto Solver<SolverImplType::qpmad>::getPrimalSolution() -> Eigen::VectorXd {
    return qp_data_.solution;
  }

  } // namespace Solver
}
// #endif