#include "Model.hpp"

TEST_P(ModelTest, NrOfDof) {
  EXPECT_TRUE(model->getNrOfDegreesOfFreedom() == 3);
}

TEST_P(ModelTest, TestLimits) {
  EXPECT_TRUE(model->getLowerJointPositionLimits().isApprox(min_pos, 1e-5));
  EXPECT_TRUE(model->getUpperJointPositionLimits().isApprox(max_pos, 1e-5));
  EXPECT_TRUE(model->getJointVelocityLimits().isApprox(max_vel, 1e-5));
  EXPECT_TRUE(model->getJointTorqueLimits().isApprox(max_torque, 1e-5));
}

TEST_P(ModelTest, getFramePose) {
  setRandomRobotState();
  PLOGD << " model->getFramePose(tool_tip).matrix()\n"
        << model->getFramePose("tool_tip").matrix();
  PLOGD << " getFramePose(robot_state.joint_position).matrix()\n"
        << getFramePose(robot_state.joint_position).matrix();
  EXPECT_TRUE(
      model->getFramePose("tool_tip")
          .matrix()
          .isApprox(getFramePose(robot_state.joint_position).matrix(), 1e-5));
}

TEST_P(ModelTest, getWrongFramePose) {
  setRandomRobotState();
  EXPECT_THROW(
      {
        try {
          model->getFramePose("wrong_frame_name");
        } catch (const qontrol_exception &e) {
          // and this tests that it has the correct message
          std::string what = e.what();
          std::string expect_string = "Frame wrong_frame_name doesn't exist";
          bool res_contains = (what.find(expect_string) != std::string::npos);
          EXPECT_TRUE(res_contains);
          throw;
        }
      },
      std::runtime_error);
}

TEST_P(ModelTest, GetLink2Pose) {
  setRandomRobotState();
  EXPECT_TRUE(model->getFramePose("link_2").matrix().isApprox(
      getLink2Pose(robot_state.joint_position).matrix(), 1e-5));
}

TEST_P(ModelTest, GetJacobian) {
  setRandomRobotState();
  EXPECT_TRUE(model->getJacobian("tool_tip")
                  .isApprox(getJacobian(robot_state.joint_position), 1e-5));
}

TEST_P(ModelTest, changeJacobianPoint) {
  setRandomRobotState();
  auto base_h_link3 = model->getFramePose("link_3");
  PLOGD << " model->getJacobian(link_3) \n" << model->getJacobian("link_3");
  PLOGD << "model->getJacobian(base_h_link3) \n"
        << model->getJacobian("tool_tip", base_h_link3);
  EXPECT_TRUE(model->getJacobian("link_3").isApprox(
      model->getJacobian("tool_tip", base_h_link3), 1e-5));
}

TEST_P(ModelTest, getVelocity) {
  setRandomRobotState();
  EXPECT_TRUE(model->getFrameVelocity("tool_tip")
                  .isApprox(getJacobian(robot_state.joint_position) *
                                robot_state.joint_velocity,
                            1e-5));
}

TEST_P(ModelTest, changeVelocity) {
  setRandomRobotState();
  auto base_hlink3 = model->getFramePose("link_3");
  EXPECT_TRUE(model->getFrameVelocity("link_3").isApprox(
      model->getFrameVelocity("tool_tip", base_hlink3), 1e-4));
}

TEST_P(ModelTest, JdotQdot) {
  setRandomRobotState();
  EXPECT_TRUE(
      model->getJacobianTimeDerivativeQdot("tool_tip")
          .isApprox(getJacobianTimeDerivative(robot_state.joint_position,
                                              robot_state.joint_velocity) *
                        robot_state.joint_velocity,
                    1e-5));
}

TEST_P(ModelTest, changeJdotQdot) {
  setRandomRobotState();
  auto base_hlink3 = model->getFramePose("link_3");
  PLOGD << "model->getJacobianTimeDerivativeQdot(link_3)\n"
        << model->getJacobianTimeDerivativeQdot("link_3");
  PLOGD << "model->getJacobianTimeDerivativeQdot(tool_tip, "
           "base_hlink3)\n"
        << model->getJacobianTimeDerivativeQdot("tool_tip", base_hlink3);
  EXPECT_TRUE(model->getJacobianTimeDerivativeQdot("link_3").isApprox(
      model->getJacobianTimeDerivativeQdot("tool_tip", base_hlink3), 1e-5));
}

TEST_P(ModelTest, Skew) {
  Eigen::Vector3d mat(0.04393398, 0.25606602, 0.15);
  Eigen::Matrix3d expected_result;
  expected_result << 0, -0.15000000000000005, 0.2560660171779823,
      0.15000000000000005, 0, -0.043933982822017895, -0.2560660171779823,
      0.043933982822017895, 0;
  EXPECT_TRUE(model->skew(mat).isApprox(expected_result, 1e-5));
}

TEST_P(ModelTest, GetGravityTorques) {
  setRandomRobotState();
  getGravityTorque(robot_state.joint_position);
  EXPECT_TRUE(model->getJointGravityTorques().isApprox(
      getGravityTorque(robot_state.joint_position), 1e-5));
}

TEST_P(ModelTest, GetCoriolisTorques) {
  setRandomRobotState();
  EXPECT_TRUE(model->getJointCoriolisTorques().isApprox(
      getCoriolisMatrix(robot_state.joint_position,
                        robot_state.joint_velocity) *
          robot_state.joint_velocity,
      1e-5));
}

TEST_P(ModelTest, InertiaMatrix) {
  setRandomRobotState();
  EXPECT_TRUE(model->getJointInertiaMatrix().isApprox(
      getJointInertiaMatrix(robot_state.joint_position), 1e-5));
}

TEST_P(ModelTest, InverseInertiaMatrix) {
  setRandomRobotState();
  EXPECT_TRUE(model->getInverseJointInertiaMatrix().isApprox(
      getJointInertiaMatrix(robot_state.joint_position).inverse(), 1e-5));
}

TEST_P(ModelTest, ExternalWrench) {
  setRandomRobotState();
  EXPECT_TRUE(
      model->getExternalWrench("tool_tip")
          .isApprox((getJacobian(robot_state.joint_position).transpose())
                        .colPivHouseholderQr()
                        .solve(robot_state.external_joint_torque),
                    1e-5));
}

TEST_P(ModelTest, ExternalWrenchFrame) {
  setRandomRobotState();
  auto base_h_link3 = model->getFramePose("link_3");
  EXPECT_TRUE(
      model->getExternalWrench("tool_tip", base_h_link3)
          .isApprox((model->getJacobian("tool_tip", base_h_link3).transpose())
                        .colPivHouseholderQr()
                        .solve(robot_state.external_joint_torque),
                    1e-5));
}

TEST_P(ModelTest, FrameAccelerationFromTorque) {
  setRandomRobotState();
  auto minv = model->getInverseJointInertiaMatrix();
  auto j = getJacobian(robot_state.joint_position);
  auto jdot = getJacobianTimeDerivative(robot_state.joint_position,
                                        robot_state.joint_velocity);
  auto coriolis = getCoriolisMatrix(robot_state.joint_position,
                                    robot_state.joint_velocity) *
                  robot_state.joint_velocity;
  auto gravity = getGravityTorque(robot_state.joint_position);
  auto qdd = minv * (robot_state.joint_torque - coriolis - gravity);
  auto xdd = jdot * robot_state.joint_velocity + j * qdd;
  PLOGD << "Model\n"
        << model->getFrameAccelerationFromTorques("tool_tip").transpose();
  PLOGD << "Analytic \n" << xdd.transpose();
  EXPECT_TRUE(
      model->getFrameAccelerationFromTorques("tool_tip").isApprox(xdd, 1e-1));
}

TEST_P(ModelTest, changeFrameAccelerationFromTorque) {
  setRandomRobotState();
  auto base_hlink3 = model->getFramePose("link_3");
  PLOGD << "model->getFrameAccelerationFromTorques(link_3)\n"
        << model->getFrameAccelerationFromTorques("link_3");
  PLOGD << "model->getFrameAccelerationFromTorques(tool_tip, "
           "base_hlink3)\n"
        << model->getFrameAccelerationFromTorques("tool_tip", base_hlink3);
  EXPECT_TRUE(model->getFrameAccelerationFromTorques("link_3").isApprox(
      model->getFrameAccelerationFromTorques("tool_tip", base_hlink3), 1e-2));
}

// TEST_P(ModelTest, createReducedModel) {
//   std::string robot_description_path = "../tests/resources/3R.urdf";
//   Qontrol::ControlType control_type = Qontrol::ControlType::joint_velocity;
//   auto test_type = GetParam();
//   Qontrol::Problem *problem;
//   if (test_type == TestModelLibrary::KDL) {
//     auto *model = new Qontrol::Model::ModelLoader(
//         Qontrol::ModelLibrary::kdl, robot_description_path, "link_1",
//         "link_2");
//     problem = new Qontrol::Problem(model, control_type);
//   } else if (test_type == TestModelLibrary::pinocchio) {
//     auto *model = new Qontrol::Model::ModelLoader(
//         Qontrol::ModelLibrary::pinocchio, robot_description_path, "link_1",
//         "link_2");
//     problem = new Qontrol::Problem(model, control_type);
//   }
//   EXPECT_TRUE(model->getNrOfDegreesOfFreedom() == 2);
// }

INSTANTIATE_TEST_CASE_P(ModelTest, ModelTest,
                        ::testing::Values(TestModelLibrary::KDL,TestModelLibrary::pinocchio),
                        ModelTest::PrintToStringParamName());
