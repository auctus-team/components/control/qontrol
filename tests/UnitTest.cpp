#include "Model.hpp"
#include <Qontrol/Qontrol.hpp>
#include <gtest/gtest.h>

auto main(int argc, char **argv) -> int {
  ::testing::InitGoogleTest(&argc, argv);
  Qontrol::Log::Logger::parseArgv(argc, argv);

  return RUN_ALL_TESTS();
}
